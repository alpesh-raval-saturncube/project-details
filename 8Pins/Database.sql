-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 22, 2016 at 04:58 AM
-- Server version: 5.5.52-cll
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mindgymk_cam8844`
--

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE IF NOT EXISTS `favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL,
  `active_flag` int(11) NOT NULL,
  `crt_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL,
  `rating` decimal(2,1) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `request_mst`
--

CREATE TABLE IF NOT EXISTS `request_mst` (
  `req_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sports_id` int(11) NOT NULL,
  `req_title` varchar(500) NOT NULL,
  `req_desc` varchar(500) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `last_updt_date` date NOT NULL,
  `crt_date` int(20) NOT NULL,
  `active_flag` int(11) NOT NULL,
  PRIMARY KEY (`req_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `req_images`
--

CREATE TABLE IF NOT EXISTS `req_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `req_id` int(11) NOT NULL,
  `image` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `sports_mst`
--

CREATE TABLE IF NOT EXISTS `sports_mst` (
  `sports_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_name` varchar(500) NOT NULL,
  `sport_color_code` varchar(100) NOT NULL,
  `type` varchar(1) NOT NULL,
  `last_updt_date` date NOT NULL,
  `crt_date` int(20) NOT NULL,
  `active_flag` int(11) NOT NULL,
  PRIMARY KEY (`sports_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `sports_mst`
--

INSERT INTO `sports_mst` (`sports_id`, `sport_name`, `sport_color_code`, `type`, `last_updt_date`, `crt_date`, `active_flag`) VALUES
(1, 'Views/Lookouts ', 'View-Lookout.png', 'N', '0000-00-00', 0, 1),
(2, 'Abandoned locations', 'Abandoned-Building.png', 'N', '0000-00-00', 0, 1),
(3, 'Haunted locations', 'Haunted-Location.png', 'N', '0000-00-00', 0, 1),
(4, 'Cliff Jumping', 'Cliff-Jumping-Location.png', 'N', '0000-00-00', 0, 1),
(5, 'Community Pool/Jacuzzi', 'Community-Pool-Location.png', 'N', '0000-00-00', 0, 1),
(6, '24 Hour Food', '24-Hour-Food-Location-LOGO.png', 'N', '0000-00-00', 0, 1),
(7, 'Tunnel/Bridge', 'Tunnel-Bridge-Location-.png', 'N', '0000-00-00', 0, 1),
(8, 'Unique Structure', 'Unique-Strcutures.png', 'N', '0000-00-00', 0, 1),
(23, 'Holiday Lights', 'Holiday-Lights-Location.png', 'S', '0000-00-00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_mst`
--

CREATE TABLE IF NOT EXISTS `user_mst` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_img` varchar(200) NOT NULL,
  `last_updt_date` date DEFAULT NULL,
  `crt_date` int(20) NOT NULL,
  `active_flag` int(1) NOT NULL,
  `email` varchar(60) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `code` varchar(20) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `connectTo` varchar(100) NOT NULL,
  `appId` varchar(150) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `last_action` int(11) NOT NULL,
  `fb_username` varchar(100) DEFAULT NULL,
  `fb_email` varchar(100) DEFAULT NULL,
  `about` text NOT NULL,
  `ios_notification` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
